import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import VueGtag from "vue-gtag";
import * as Sentry from "@sentry/vue";
import './style.scss';
import 'vuetify/styles';
import { createVuetify } from 'vuetify';
import * as components from 'vuetify/components';
import * as directives from 'vuetify/directives';
import '@mdi/font/css/materialdesignicons.css';

const { VITE_GA_MEASUREMENT_ID, VITE_SENTRY_DNS } = import.meta.env;

const app = createApp(App);

app.use(router);

Sentry.init({
  app,
  dsn: VITE_SENTRY_DNS,
  integrations: [
    new Sentry.BrowserTracing({
      routingInstrumentation: Sentry.vueRouterInstrumentation(router),
    }),
    new Sentry.Replay(),
  ],
  tracesSampleRate: 1.0,
  replaysSessionSampleRate: 0.1,
  replaysOnErrorSampleRate: 1.0,
});

// Vuetify
const vuetify = createVuetify({
  components,
  directives,
  icons: {
    defaultSet: 'mdi'
  }
});
app.use(vuetify);

// gtag
app.use(VueGtag, {
  config: { id: VITE_GA_MEASUREMENT_ID }
}, router);

app.mount('#app');
