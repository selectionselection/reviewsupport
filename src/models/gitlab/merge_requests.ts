import { parseYamlFromText, parseTextFromYaml } from '@/assets/parse-yaml';

export type MRState = 'opened' | 'merged' | 'closed';
export type reviewState = 'ok' | 'ng' | 'ignore';
export type reviewType = '' | 'requirement' | 'soft_design' | 'coding' | 'unit_test' | 'integration_test';
export type reviewerType = 'self_review' | 'review' | 'outsider_review';
export type reviewInfo = {
  title: string,
  state: reviewState
};

export type reviewDetail = {
  type: reviewType,
  self_reviewed_list: Array<reviewInfo>,
  reviewed_list: Array<reviewInfo>,
  outsider_reviewed_list: Array<reviewInfo>
};

export class MergeRequests {
  id: number;
  iid: number;
  project_id: number;
  state: MRState;
  title: string;
  description: string;
  draft: boolean;
  web_url: string;
  updated_at: string;

  // yamlから読み取る系データ
  type: reviewType;
  self_reviewed_list: Array<reviewInfo>;
  reviewed_list: Array<reviewInfo>;
  outsider_reviewed_list: Array<reviewInfo>;

  constructor(init: Partial<MergeRequests>) {
    this.id = init.id ?? 0;
    this.iid = init.iid ?? 0;
    this.project_id = init.project_id ?? 0;
    this.draft = init.draft ?? false;
    this.state = init.state ?? 'opened';
    this.title = init.title ?? '';
    this.description = init.description ?? '';
    this.web_url = init.web_url ?? '';
    this.updated_at = init.updated_at ?? '';

    this.type = '';
    this.self_reviewed_list = [];
    this.reviewed_list = [];
    this.outsider_reviewed_list = [];

    // 本文からyamlのデータを取得する
    const detail = parseYamlFromText(this.description) as reviewDetail;

    if (detail) {
      this.type = detail.type ?? '';
      this.self_reviewed_list = detail.self_reviewed_list ?? [];
      this.reviewed_list = detail.reviewed_list ?? [];
      this.outsider_reviewed_list = detail.outsider_reviewed_list ?? [];
    }
  }

  checkedReviewList(reviewerType: reviewerType, state: reviewState) {
    if (reviewerType == 'self_review') {
      return this.self_reviewed_list.filter((item) => {
        return item.state == state;
      });
    } else if (reviewerType == 'review') {
      return this.reviewed_list.filter((item) => {
        return item.state == state;
      });
    } else if (reviewerType == 'outsider_review') {
      return this.outsider_reviewed_list.filter((item) => {
        return item.state == state;
      });
    }

    return [];
  }

  updateBasicInfo(type: reviewType) {
    this.type = type;
  }

  updateReviewed(reviewerType: reviewerType, reviewed: reviewInfo[]) {
    if (reviewerType == 'self_review') {
      this.self_reviewed_list = reviewed;
    } else if (reviewerType == 'review') {
      this.reviewed_list = reviewed;
    } else if (reviewerType == 'outsider_review') {
      this.outsider_reviewed_list = reviewed;
    }
  }

  getJsonData() {
    const yaml_obj = {
      type: this.type,
      self_reviewed_list: this.self_reviewed_list,
      reviewed_list: this.reviewed_list,
      outsider_reviewed_list: this.outsider_reviewed_list
    };

    this.description = parseTextFromYaml(
      this.description,
      yaml_obj
    );

    return JSON.stringify({
      description: this.description
    });
  }

}
