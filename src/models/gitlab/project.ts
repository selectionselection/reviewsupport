import { MergeRequests } from '@/models/gitlab/merge_requests';

export class Project {
  id: number;
  name: string;
  name_with_namespace: string;
  web_url: string;
  merge_requests: MergeRequests[];

  constructor(init: Partial<Project>) {
    this.id = init.id ?? 0;
    this.name = init.name ?? '';
    this.name_with_namespace = init.name_with_namespace ?? '';
    this.web_url = init.web_url ?? '';
    this.merge_requests = [];
  }

  ProjectReviewedMergeRate() {
    const list = this.merge_requests.filter((item) => {
      return item.type != '';
    });

    const reviewed = list.filter((item) => {
      return item.reviewed_list.length > 0;
    });

    if (list.length > 0) {
      return (reviewed.length / list.length) * 100;
    } else {
      return 0;
    }
  }

  ProjectWaitReviewMergeRequest() {
    const opened = this.merge_requests.filter((item) => {
      return item.state == 'opened';
    });

    const waitReview = opened.filter((item) => {
      return item.reviewed_list.length == 0;
    });

    try {
      return waitReview.length;
    } catch {
      return 0;
    }
  }

  ProjectNGMergeRequest() {
    const opened = this.merge_requests.filter((item) => {
      return item.state == 'opened';
    });

    const ng = opened.filter((item) => {
      return item.checkedReviewList('self_review', 'ng').length > 0 ||
        item.checkedReviewList('review', 'ng').length > 0 ||
        item.checkedReviewList('outsider_review', 'ng').length > 0;
    });

    try {
      return ng.length;
    } catch {
      return 0;
    }
  }
}
