export class User {
  id: number;
  username: string;
  name: string;
  state: string;
  avatar_url: string;
  web_url: string;

  constructor(init: Partial<User>) {
    this.id = init.id ?? 0;
    this.username = init.username ?? '';
    this.name = init.name ?? '';
    this.state = init.state ?? '';
    this.avatar_url = init.avatar_url ?? '';
    this.web_url = init.web_url ?? '';
  }
}
