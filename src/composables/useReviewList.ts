import { watch } from 'vue';

import useUser from '@/composables/useUser';
import useGitLab from '@/composables/useGitLab';

const { logined } = useUser();
const {
  projects,
  getProjects,
  getMergeRequest
} = useGitLab();

export default () => {
  const init = () => {
    if (logined) {
      getProjects();
    }
  };

  /**
   * プロジェクトが追加されるごとにMRを取得する。
   */
  watch(() => [...projects.value], (next, prev) => {
    const diff = next.filter(i => prev.indexOf(i) == -1);

    for (const item of diff) {
      getMergeRequest(item);
    }
  });

  return {
    init,
    projects
  };
};
