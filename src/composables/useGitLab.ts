import { ref } from 'vue';
import type { Ref } from 'vue';

import { MergeRequests, MRState, User, Project } from '@/models/gitlab';
import useUser from '@/composables/useUser';

type Pagenation = {
  page: number,
  totalPage: number,
  data: object[]
}

const { VITE_GITLAB_BASE_DOMAIN} = import.meta.env;
const baseUrl: string = VITE_GITLAB_BASE_DOMAIN;

const projects: Ref<Array<Project>> = ref([]);

export default () => {
  const headers = (authToken: string): Record<string, string> => {
    const data: Record<string, string> = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${authToken}`
    };

    return data;
  };

  const getPagenartionData = async (
    url: string,
    params: object,
    page: number,
    totalPage: number
  ): Promise<Pagenation> => {
    const reqParams = {
      ...params,
      page: page.toString(),
      per_page: '25'
    };
    const response = await fetch(url + '?' + (new URLSearchParams(reqParams)), {
      method: 'GET',
      headers: headers(useUser().access_token.value)
    })
      .then(response => {
        totalPage = Number(response.headers.get('X-Total-Pages'));
        return response.json();
      });

    page = page + 1;

    return {
      page: page,
      totalPage: totalPage,
      data: response
    };
  };

  const getUserInfo = async () => {
    const url = baseUrl + '/api/v4/user';

    try {
      const user = await fetch(url, {
        method: 'GET',
        headers: headers(useUser().access_token.value)
      })
        .then(response => {
          return response.json();
        });

      return new User(user);
    } catch (error) {
      return new User({});
    }
  };

  const getProject = async (id: number) => {
    const url = `${baseUrl}/api/v4/projects/${id}`;

    const response = await fetch(url, {
      method: 'GET',
      headers: headers(useUser().access_token.value)
    })
      .then(response => {
        return response.json();
      });

    return new Project(response);
  };

  /**
   * プロジェクト一覧を取得する
   */
  const getProjects = async () => {
    projects.value = []; // クリア

    const url = `${baseUrl}/api/v4/projects`;
    const params = {
      membership: true,
      with_merge_requests_enabled: true
    };
    let page = 1;
    let totalPage = 1;

    do {
      const pagenation = await getPagenartionData(
        url,
        params,
        page,
        totalPage
      );

      page = pagenation.page;
      totalPage = pagenation.totalPage;
      for (const item of pagenation.data) {
        // クラスに置換する
        projects.value.push(new Project(item));
      }

    } while (page <= totalPage);
  };

  const getMergeRequest = async (project: Project, state: MRState='opened') => {
    const url = `${baseUrl}/api/v4/projects/${project.id}/merge_requests`;
    const params = {
      state: state
    };
    let page = 1;
    let totalPage = 1;

    do {
      const pagenation = await getPagenartionData(
        url,
        params,
        page,
        totalPage
      );

      page = pagenation.page;
      totalPage = pagenation.totalPage;
      for (const item of pagenation.data) {
        // クラスに置換する
        project.merge_requests.push(new MergeRequests(item));
      }

    } while (page <= totalPage);
  };

  const updateMergeRequest = async (mr: MergeRequests) => {
    const url = baseUrl + `/api/v4/projects/${mr.project_id}/merge_requests/${mr.iid}`;

    try {
      await fetch(url, {
        method: 'PUT',
        headers: headers(useUser().access_token.value),
        body: mr.getJsonData()
      })
        .then(response => {
          return response.json();
        });

        return true;
    } catch (error) {
      return false;
    }
  };

  return {
    projects,
    getUserInfo,
    getProject,
    getProjects,
    getMergeRequest,
    updateMergeRequest
  };
};