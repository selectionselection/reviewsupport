import { default as dayjs } from 'dayjs';
import type { ConfigType } from 'dayjs';
import 'dayjs/locale/ja';
import isBetween from 'dayjs/plugin/isBetween';
import isSameOrAfter from 'dayjs/plugin/isSameOrAfter';
import isSameOrBefore from 'dayjs/plugin/isSameOrBefore';

dayjs.extend(isBetween);
dayjs.extend(isSameOrAfter);
dayjs.extend(isSameOrBefore);

export default (params: ConfigType | Array<number>) => {
  if (Array.isArray(params)) {
    const datetimeArray = [...params, 0, 0, 0, 0, 0, 0, 0];
    const date = new Date(
      datetimeArray[0],
      datetimeArray[1],
      datetimeArray[2],
      datetimeArray[3],
      datetimeArray[4],
      datetimeArray[5],
      datetimeArray[6]
    );

    return dayjs(date).locale('ja');
  } else {
    return dayjs(params).locale('ja');
  }
};
