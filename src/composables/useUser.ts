import { ref, computed } from 'vue';
import type { Ref } from 'vue';
import useGitLab from '@/composables/useGitLab';
import useOAuth2 from '@/composables/useOAuth2';
import { User } from '@/models/gitlab';

const access_token: Ref<string> = ref('');
const user: Ref<User> = ref(new User({}));

export default () => {
  // 初期処理
  // ローカルストレージからtokenを取得する
  access_token.value = localStorage.getItem("access_token") ?? '';

  /**
   * ユーザ情報の取得
   */
  const syncUserInfo = async () => {
    await useOAuth2().refresh();
    user.value = await useGitLab().getUserInfo();
  };

  const logined = computed(() => {
    return user.value.id != 0;
  });

  return {
    access_token,
    user,
    logined,
    syncUserInfo
  };
};
