import { ref, computed } from 'vue';
import type { Ref } from 'vue';
import { Project, MergeRequests } from '@/models/gitlab';
import useGitLab from '@/composables/useGitLab';

const { getProject, getMergeRequest } = useGitLab();
const project: Ref<Project> = ref(new Project({}));
const target_mr_iid = ref(0);

export default () => {
  const init = async (id: number) => {
    project.value = await getProject(id);

    await getMergeRequest(project.value, 'opened');
    await getMergeRequest(project.value, 'merged');
    await getMergeRequest(project.value, 'closed');
  };

  const merge_request = computed(() => {
    const mr = project.value.merge_requests.filter((item) => {
      return item.iid == target_mr_iid.value;
    });

    if (mr.length == 1) {
      return mr[0];
    } else {
      return new MergeRequests({});
    }
  });

  return {
    project,
    target_mr_iid,
    merge_request,
    init
  };
};
