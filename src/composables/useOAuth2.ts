type Token = {
  access_token: string,
  refresh_token: string
}

const {
  VITE_GITLAB_BASE_DOMAIN,
  VITE_GITLAB_APP_ID,
  VITE_GITLAB_APP_SECRET,
  VITE_GITLAB_REDIRECT_URI,
} = import.meta.env;

const createFormParams = (params: Record<string, string>) => {
  return Object.keys(params)
    .map((key) => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`)
    .join('&');
};


export default () => {
  const redirect = () => {
    const params = {
      client_id: VITE_GITLAB_APP_ID ?? '',
      redirect_uri: VITE_GITLAB_REDIRECT_URI ?? '',
      response_type: 'code',
      state: (Math.floor(Math.random() * (1000 + 1 - 0))).toString()
    };
    const urlSearchParam = new URLSearchParams(params).toString();

    window.location.href = `${VITE_GITLAB_BASE_DOMAIN}/oauth/authorize?${urlSearchParam}`;
  };

  const callback = async (code: string) => {
    const params = {
      client_id: VITE_GITLAB_APP_ID ?? '',
      client_secret: VITE_GITLAB_APP_SECRET ?? '',
      code: code ?? '',
      grant_type: 'authorization_code',
      redirect_uri: VITE_GITLAB_REDIRECT_URI ?? ''
    };

    const token_info: Token = await fetch(`${VITE_GITLAB_BASE_DOMAIN}/oauth/token`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: createFormParams(params)
    })
      .then(response => {
        return response.json();
      });

    localStorage.setItem('access_token', token_info.access_token);
    localStorage.setItem('refresh_token', token_info.refresh_token);
  };

  const refresh = async () => {
    const token = localStorage.getItem("refresh_token") ?? '';

    const params = {
      client_id: VITE_GITLAB_APP_ID ?? '',
      client_secret: VITE_GITLAB_APP_SECRET ?? '',
      grant_type: 'refresh_token',
      refresh_token: token,
      redirect_uri: VITE_GITLAB_REDIRECT_URI ?? ''
    };

    const token_info: Token = await fetch(`${VITE_GITLAB_BASE_DOMAIN}/oauth/token`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: createFormParams(params)
    })
      .then(response => {
        return response.json();
      });

    localStorage.setItem('access_token', token_info.access_token);
    localStorage.setItem('refresh_token', token_info.refresh_token);
  };

  return {
    redirect,
    callback,
    refresh
  };
};
