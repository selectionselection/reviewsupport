import yaml from 'js-yaml';

export const parseYamlFromText = (text: string) => {
  // 本文からyamlのデータを取得する
  let yaml_part = text.split(/^```yaml/);
  if (yaml_part === null || yaml_part.length < 2) {
    return null;
  }

  yaml_part = yaml_part[1].split(/```/);
  if (yaml_part === null || yaml_part.length < 2) {
    return null;
  }

  try {
    return yaml.load(yaml_part[0]) as object;
  } catch (e) {
    return null;
  }
};

export const parseTextFromYaml = (text: string, data: object) => {
  // 本文からyaml部分を一旦削除する
  let yaml_part = text.split(/^```yaml/);
  if (yaml_part.length >= 2) {
    yaml_part = yaml_part[1].split(/```/);
    if (yaml_part.length >= 2) {
      text = text.replace(
        '```yaml' + yaml_part[0] + '```',
        ''
      );
    }
  }

  return '```yaml\n' + yaml.dump(data) + '```\n' + text;
};
