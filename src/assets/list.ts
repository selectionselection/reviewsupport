export type Item = {
  type: string[],
  category: string[],
  label: string,
  value: string,
  file: string,
}
export const datas: Array<Item> = [
  {
    type: [],
    category: ['self_review', 'review', 'outsider_review'],
    label: 'マージリクエストの説明は十分か',
    value: 'マージリクエストの説明は十分か',
    file: 'マージリクエストの説明は十分か.md'
  },
  {
    type: [],
    category: ['self_review', 'review', 'outsider_review'],
    label: 'スペルミスがないか',
    value: 'スペルミスがないか',
    file: 'スペルミスがないか.md'
  },
  {
    type: [],
    category: ['self_review', 'review', 'outsider_review'],
    label: 'パスワード、秘密鍵などは埋め込まれていないか',
    value: 'パスワード、秘密鍵などは埋め込まれていないか',
    file: 'パスワード、秘密鍵などは埋め込まれていないか.md'
  },
  {
    type: [],
    category: ['self_review', 'review', 'outsider_review'],
    label: '横展開調査したか',
    value: '横展開調査したか',
    file: '横展開調査したか.md'
  },
  {
    type: ['coding'],
    category: ['self_review', 'review', 'outsider_review'],
    label: 'マジックナンバーはないか',
    value: 'マジックナンバーはないか',
    file: 'マジックナンバーはないか.md'
  },
  {
    type: ['coding'],
    category: ['self_review', 'review', 'outsider_review'],
    label: '既存の共通処理があることを見逃していないか',
    value: '既存の共通処理があることを見逃していないか',
    file: '既存の共通処理があることを見逃していないか.md'
  },
  {
    type: ['coding'],
    category: ['self_review', 'review', 'outsider_review'],
    label: 'デッドコードや意味のないコメントアウトが残っていないか',
    value: 'デッドコードや意味のないコメントアウトが残っていないか',
    file: 'デッドコードや意味のないコメントアウトが残っていないか.md'
  },
  {
    type: ['coding'],
    category: ['self_review', 'review', 'outsider_review'],
    label: 'コメントがコードと食い違っていないか',
    value: 'コメントがコードと食い違っていないか',
    file: 'コメントがコードと食い違っていないか.md'
  }
];
