import {
    createRouter,
    createWebHistory,
    RouteRecordRaw
  } from 'vue-router';
  import useUser from '@/composables/useUser';
  
  const { user } = useUser();
  
  const DEFAULT_TITLE = 'ReviewSupport';
  
  const routes: Array<RouteRecordRaw> = [
    {
      path: '/',
      component: () => import('@/pages/index.vue'),
    },
    {
      path: '/login',
      component: () => import('@/pages/login.vue'),
    },
    {
      path: '/reviewpoint',
      name: 'レビュー観点一覧',
      component: () => import('@/pages/reviewpoint/index.vue'),
    },
    {
      path: '/p/:id',
      name: 'プロジェクト',
      component: () => import('@/pages/p/_id/index.vue'),
    },
    {
      path: '/p/:id/m/:iid',
      name: 'マージリクエスト',
      component: () => import('@/pages/p/_id/m/_iid/index.vue'),
    },
    {
      path: '/:catchAll(.*)',
      name: 'NotFoundError',
      component: () => import('@/pages/404.vue'),
      meta: {
        title: '404'
      },
    }
  
  ];
  
  const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: routes
  });
  
  // 認証のルート管理
  router.beforeEach((to, from, next) => {
    try {
      if (to.meta.requiresAuth && user.value.id == 0) {
        next('/');
      } else {
        next();
      }
    } catch {
      next(from.path);
    }
  });
  
  router.afterEach((to) => {
    const title = to.meta.title ? `${to.meta.title} | ${DEFAULT_TITLE}` : `${DEFAULT_TITLE}`;
    document.title = title;
  });
  
  
  export default router;
  