/// <reference types="vitest" />

import { defineConfig, loadEnv, UserConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import path from 'path';

// https://vitejs.dev/config/
export default defineConfig(({ mode }): UserConfig => {
  process.env = {...process.env, ...loadEnv(mode, process.cwd())};
  return {
      base: process.env.VITE_APP_PATH,
      build: {
        sourcemap: true,
      },
      plugins: [vue()],
      resolve: {
        alias: {
          '@/': path.join(__dirname, './src/'),
        }
      },
      assetsInclude: [
        '**/*.md'
      ],
      test: {
        globals: true,
        coverage: {
          all: true,
          include: [
            "src/**/*.js",
            "src/**/*.ts",
            "src/**/*.d.ts",
            "src/**/*.tsx"
          ]
        }
      }
    };
});
